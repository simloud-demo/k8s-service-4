#!/bin/bash
docker buildx create --name buildx --use
aws ecr get-login-password \
  --region $CLUSTER_REGION | \
  docker login \
    --username AWS \
    --password-stdin 322219090568.dkr.ecr.$CLUSTER_REGION.amazonaws.com
docker buildx build \
  -t $ECR_REPOSITORY \
  --cache-from=type=registry,ref=$ECR_REPOSITORY:cache \
  --cache-to=mode=max,image-manifest=true,oci-mediatypes=true,type=registry,ref=$ECR_REPOSITORY:cache \
  --push \
  .
docker pull $ECR_REPOSITORY:latest && docker tag $ECR_REPOSITORY:latest $DOCKER_IMAGE_NAME:latest
